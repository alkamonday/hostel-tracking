package in.mondays.hostel.Activity;

import android.app.ProgressDialog;
import android.app.admin.SystemUpdatePolicy;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import in.mondays.hostel.Firebase.PersistentDatabase;
import in.mondays.hostel.GetterSetter.Groups;
import in.mondays.hostel.R;
import in.mondays.hostel.Utils.BaseUrls;
import in.mondays.hostel.Utils.CommonMethods;
import in.mondays.hostel.Utils.Global;
import in.mondays.hostel.Utils.Validations;

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_verify;
    EditText veri_Number;
    SharedPreferences sp;
    private FirebaseAuth mAuth;
    ProgressDialog dialog;
    SharedPreferences loginSp;

    DatabaseReference adminDb;
    ValueEventListener adminListener;
    AlertDialog.Builder alertdialog;
    AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        mAuth=FirebaseAuth.getInstance();

        inti();
    }

    private void inti() {
        getSupportActionBar().hide();
        btn_verify= (Button) findViewById(R.id.btn_verification_verify);
        veri_Number = (EditText) findViewById(R.id.et_verificationMobile);
        mAuth = FirebaseAuth.getInstance();

        btn_verify.setOnClickListener(this);
        loginSp=getSharedPreferences(Global.LOGIN_SP,MODE_PRIVATE);
    }

    @Override
    public void onClick(View view) {
        if (veri_Number.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), "Code can't be blank!", Toast.LENGTH_SHORT).show();
            veri_Number.setHintTextColor(Color.RED);
        }else if(!Validations.isNumberValid(veri_Number.getText().toString().trim(), 10)){
            Toast.makeText(getApplicationContext(), "Invalid Code!", Toast.LENGTH_SHORT).show();
            veri_Number.setHintTextColor(Color.RED);
        }else {
            veri_Number.setHintTextColor(ContextCompat.getColor(VerificationActivity.this, R.color.textColor));
            if (CommonMethods.isInternetWorking(VerificationActivity.this)) {
                dialog = new ProgressDialog(VerificationActivity.this);
                dialog.show();
                dialog.setMessage("Verifying...");
                mAuth.signInWithEmailAndPassword(BaseUrls.AUTH_EMAIL, BaseUrls.AUTH_PASSWORD).
                        addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (!task.isSuccessful()) {
                                    System.out.println("not working ");
                                } else {
                                    isUserRegistered(getApplicationContext());
                                }
                            }
                        });
            }else {
                Toast.makeText(this, Global.NO_INTERNET_MSG, Toast.LENGTH_SHORT).show();
            }

        }

    }

    private void isUserRegistered(final Context context) {
        final ArrayList<Groups> groupList = new ArrayList<>();
        sp = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        int versionCode=0;
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            sp.edit().putString(Global.VERSION_NUMBER,""+versionCode).commit();

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        DatabaseReference dRef= PersistentDatabase.getDatabase().getReference("biz/");
        dRef.keepSynced(true);
        dRef.orderByChild("code").equalTo(veri_Number.getText().toString()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot mainSnapshot) {
                        Log.v("Verification", mainSnapshot.toString());
                        if (mainSnapshot != null && mainSnapshot.getValue() != null) {
                            for (DataSnapshot dataSnapshot : mainSnapshot.getChildren()) {
                                if (dataSnapshot != null) {
                                    loginSp.edit().putString(Global.BIZ_ID , dataSnapshot.getKey()).apply();
                                    loginSp.edit().putString(Global.BIZ_COMPANY_NAME,dataSnapshot.child("nm").getValue(String.class)).apply();
                                    loginSp.edit().putString(Global.TYP,dataSnapshot.child("typ").getValue(String.class)).apply();

                                    for (DataSnapshot grpChild : dataSnapshot.child("grp").getChildren()) {

                                        Groups groups = new Groups();
                                        groups.setGroupId(grpChild.getKey());
                                        groups.setGroupName(grpChild.child("nm").getValue().toString());
                                        groupList.add(groups);
                                        dialog.dismiss();


                                    } if (groupList.size() > 1) {
                                        openDialogForGroupSelection(groupList, dataSnapshot, context);

                                    } else {
                                        SharedPreferences.Editor editor = loginSp.edit();
                                        editor.putString(Global.GROUP_ID, groupList.get(0).getGroupId().toString());
                                        editor.putString(Global.GROUP_NAME, groupList.get(0).getGroupName().toString());
                                        if (dataSnapshot.hasChild("expDt"))
                                            editor.putString(Global.BIZ_EXPIRY_DATE, dataSnapshot.child("expDt").getValue().toString());

                                        if (dataSnapshot.hasChild("img"))
                                            editor.putString(Global.BIZ_COMPANY_IMG, dataSnapshot.child("img").getValue().toString());
                                        editor.apply();
                                        String uuid =Settings.Secure.getString(getContentResolver(),
                                                Settings.Secure.ANDROID_ID);

                                        loginSp.edit().putString(Global.DEVICE_ID, uuid).apply();

                                        updateDevice(true,CommonMethods.getCurrentTime("hh:mm a"),false,FirebaseInstanceId.getInstance().getToken(),CommonMethods.getVerisonNumber(context),Calendar.getInstance().getTimeInMillis(),"hostel",groupList.get(0).getGroupId().toString());


                                        dialog.dismiss();
                                        Intent in = new Intent(getApplicationContext(), StudentActivity.class);
                                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        loginSp.edit().putBoolean(Global.IS_LOGGED_IN, true).apply();
                                        finish();
                                        startActivity(in);

                                    }
//                                    if (groupList.size() > 1) {
//
//
//                                    } else {
//                                        SharedPreferences.Editor editor = loginSp.edit();
//                                        editor.putString(Global.GROUP_ID, groupList.get(0).getGroupId().toString());
//                                        editor.putString(Global.GROUP_NAME, groupList.get(0).getGroupName().toString());
//                                        editor.putString(Global.GROUP_NAME, groupList.get(0).getGroupName().toString());
//                                        if (dataSnapshot.hasChild("expDt"))
//                                            editor.putString(Global.BIZ_EXPIRY_DATE, dataSnapshot.child("expDt").getValue().toString());
//
//                                        if (dataSnapshot.hasChild("img"))
//                                            editor.putString(Global.BIZ_COMPANY_IMG, dataSnapshot.child("img").getValue().toString());
//
//                                        editor.apply();
//
//
//                                        String uuid =
//                                                Settings.Secure.getString(getContentResolver(),
//                                                        Settings.Secure.ANDROID_ID);
//                                        loginSp.edit().putString(Global.DEVICE_ID, uuid).apply();
//
//                                        try {
//                                            DatabaseReference deviceRef = PersistentDatabase.getDatabase().getReference("device/" + loginSp.getString(Global.BIZ_ID, "") + "/" + uuid);
//
//                                            HashMap<String, Object> map = new HashMap<String, Object>();
//
//                                            map.put("status", true);
//                                            map.put("del", false);
//                                            map.put("token", FirebaseInstanceId.getInstance().getToken());
//                                            map.put("ver", CommonMethods.getVerisonNumber(context));
//                                            map.put("dt", Calendar.getInstance().getTimeInMillis());
//                                            map.put("app","tracking");
//                                            deviceRef.updateChildren(map);
//                                            deviceRef.child("gId").setValue(groupList.get(0).getGroupId().toString());
//                                        } catch (Exception e) {
//                                            Toast.makeText(context, Global.SERVER_ERROR_MSG, Toast.LENGTH_SHORT).show();
//                                        }
//                                        dialog.dismiss();
//                                        Intent in = new Intent(getApplicationContext(), StudentActivity.class);
//                                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        loginSp.edit().putBoolean(Global.IS_LOGGED_IN, true).apply();
//                                        finish();
//                                        startActivity(in);
//
//                                    }

                                }else {
                                    dialog.dismiss();
                                    Toast toast = Toast.makeText(VerificationActivity.this, "Invalid code!", Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();
                                }
                            }

                        }
                        else
                        {
                            dialog.dismiss();
                            Toast toast = Toast.makeText(VerificationActivity.this, "Invalid code!", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

    }

    private void updateDevice(boolean b, String currentTime, boolean b1, String token, String verisonNumber, long timeInMillis, String hostel, String s) {

        String uuid =Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        try {
        DatabaseReference deviceRef = PersistentDatabase.getDatabase().getReference("device/" + loginSp.getString(Global.BIZ_ID, "") + "/hostel/" + uuid);

        HashMap<String, Object> map = new HashMap<String, Object>();


        map.put("status", b);
        map.put("nm",currentTime);
        map.put("del", b1);
        map.put("token",token);
        map.put("ver", verisonNumber);
        map.put("dt",timeInMillis);
        map.put("app",hostel);
        deviceRef.updateChildren(map);
        deviceRef.child("gId").setValue(s);
    } catch (Exception e) {
        Toast.makeText(getApplicationContext(), Global.SERVER_ERROR_MSG, Toast.LENGTH_SHORT).show();
    }

    }

    public void openDialogForGroupSelection(final ArrayList<Groups> groupList, final DataSnapshot dataSnapshot, final Context context)
    {
        android.app.AlertDialog.Builder b = new android.app.AlertDialog.Builder(this);
        b.setTitle("Select Group");
        final String[] groupListName = new String[groupList.size()];
        for(int i=0;i<groupList.size();i++)
            groupListName[i] =groupList.get(i).getGroupName();

        b.setSingleChoiceItems(groupListName, 0, new  DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    loginSp.edit().putString(Global.GROUP_ID,groupList.get(i).getGroupId().toString()).commit();
                    loginSp.edit().putString(Global.BIZ_PERSON_NAME, dataSnapshot.child("person").child("nm").getValue().toString()).commit();

                    SharedPreferences.Editor editor = loginSp.edit();

                    editor.putString(Global.GROUP_NAME,groupListName[i]).apply();
                    if(dataSnapshot.hasChild("expDt"))
                        editor.putString(Global.BIZ_EXPIRY_DATE, dataSnapshot.child("expDt").getValue().toString());
                    if(dataSnapshot.hasChild("img"))
                        editor.putString(Global.BIZ_COMPANY_IMG, dataSnapshot.child("img").getValue().toString());
                    editor.commit();

                    String uuid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    loginSp.edit().putString(Global.DEVICE_ID,uuid).commit();

                    updateDevice(true,CommonMethods.getCurrentTime("hh:mm a"),false,FirebaseInstanceId.getInstance().getToken(),CommonMethods.getVerisonNumber(context),Calendar.getInstance().getTimeInMillis(),"hostel",groupList.get(i).getGroupId().toString());

//                    DatabaseReference deviceRef = PersistentDatabase.getDatabase().
//                            getReference("device/" + loginSp.getString(Global.BIZ_ID, "") + "/hostel/" + uuid);
//                    HashMap<String, Object> map = new HashMap<String, Object>();
//                    map.put("status", true);
//                    map.put("nm",CommonMethods.getCurrentTime("hh:mm a"));
//                    map.put("del", false);
//                    map.put("token", FirebaseInstanceId.getInstance().getToken());
//                    map.put("ver", CommonMethods.getVerisonNumber(context));
//                    map.put("dt", Calendar.getInstance().getTimeInMillis());
//                    map.put("app","hostel");
//                    deviceRef.updateChildren(map);
//                    deviceRef.child("gId").setValue(groupList.get(i).getGroupId().toString());
                }
                catch (Exception e)
                {
                    System.out.println("exception is "+e.toString());
                }

                //          getMenuItems(dialogInterface);


                Intent in = new Intent(getApplicationContext(), StudentActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                loginSp.edit().putBoolean(Global.IS_LOGGED_IN,true).apply();
                finish();
                startActivity(in);
                dialogInterface.dismiss();


            }
        });
        b.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkVersion();
    }



    private void checkVersion()
    {
        adminDb= PersistentDatabase.getDatabase().getReference("admin/app/"+Global.APP_NAME);
        adminListener = adminDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot child) {
                try {
                    PackageInfo pInfo = null;
                    try {
                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    String version = pInfo.versionName;
                    SharedPreferences loginSp = getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
                    loginSp.edit().putString(Global.VERSION_NUMBER, version).apply();


                    if (!child.child("ver").getValue().toString().equalsIgnoreCase(version))
                        showAlert(child, version);
                    else {
                        if (alert != null && alert.isShowing()) {

                            alert.dismiss();
                            alert.cancel();
                        }

                    }
                }
                catch (Exception e)
                {
                    FirebaseCrash.report(e);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void showAlert(DataSnapshot child, String version)
    {
        if(alert!=null && alert.isShowing())
        {

            alert.dismiss();
            alert.cancel();
        }

        alertdialog= new AlertDialog.Builder(this);
        alertdialog.setTitle("warning!");
        alertdialog.setMessage(child.child("msg").getValue().toString());
        alertdialog.setCancelable(false);
        alertdialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //    finish();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=in.mondays.hostel"));
                startActivity(intent);

            }
        });
        alert = alertdialog.create();


        alert.show();
    }
    @Override
    protected void onPause() {
        super.onPause();

        adminDb.removeEventListener(adminListener);
    }




}
