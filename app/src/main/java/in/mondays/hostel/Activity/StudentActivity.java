package in.mondays.hostel.Activity;


import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.mondays.hostel.Adapter.ViewPagerAdapter;
import in.mondays.hostel.Firebase.PersistentDatabase;
import in.mondays.hostel.Fragment.StudentEntryFragment;
import in.mondays.hostel.R;
import in.mondays.hostel.Utils.CommonMethods;
import in.mondays.hostel.Utils.Global;
import in.mondays.nfclibrary.NFCAsyncTaskCompleteListner;
import in.mondays.nfclibrary.NFCReaderWriter;

public class StudentActivity extends AppCompatActivity implements NFCAsyncTaskCompleteListner{

    ViewPager pager;
    TabLayout tabLayout;
    int num_of_tabs = 2;
    ViewPagerAdapter viewPagerAdapter;
    String tab1, tab2;
    ArrayList<String> titles;
    String scanContent;
    SharedPreferences loginSP;
    String bizId, grpId;
    SharedPreferences spForSettings;
    StudentEntryFragment frag;
    SharedPreferences imgRef;
    TextView tv_ver_Dtl;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    //private ImageLoaderOriginal imageLoaderOriginal;
    private TextView tv_businessName, tv_groupName, tv_deviceName;
    private ImageView iv_businessLogo;
    private DatabaseReference bizRef, deviceRef;
    private ChildEventListener bizRefListener, deviceRefListener;
    private boolean isTypeIsSchool = false;
    private NFCReaderWriter nfcReaderWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        imgRef = getSharedPreferences(Global.IMAGE_SP, Context.MODE_PRIVATE);
        loginSP = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        spForSettings = getSharedPreferences(Global.SETTINGS, MODE_PRIVATE);
        bizId = loginSP.getString(Global.BIZ_ID, "");
        grpId = loginSP.getString(Global.GROUP_ID, "");

        if (loginSP.getString(Global.TYP, "").equals("school")) {
            isTypeIsSchool = true;
        } else {
            isTypeIsSchool = false;
        }
        getSettingValues();
        reSendImagesToStorage(imgRef);
        initGUI();
        nfcReaderWriter = new NFCReaderWriter(StudentActivity.this,loginSP);
    }

    private void getFirebaseData() {


        deviceRef = PersistentDatabase.getDatabase().getReference("device/" + loginSP.getString(Global.BIZ_ID, "")
                + "/" + Global.APP_NAME
                + "/" + loginSP.getString(Global.DEVICE_ID, ""));

        if (deviceRefListener != null) {
            deviceRef.removeEventListener(deviceRefListener);
        }

        deviceRefListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getKey().equals("nm")) {
                    loginSP.edit().putString(Global.DEVICE_NAME, dataSnapshot.getValue(String.class)).apply();
                    tv_deviceName.setText(dataSnapshot.getValue(String.class));
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getKey().equals("status")) {
                    try {
                        if (loginSP.getBoolean(Global.IS_LOGGED_IN, false) && dataSnapshot != null &&
                                !Boolean.parseBoolean(dataSnapshot.getValue().toString()))

                        {
                            if (dataSnapshot.getValue().toString().equals("false")) {
                                Intent in = new Intent(StudentActivity.this, VerificationActivity.class);
                                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(in);
                                finish();
                            }

                        }
                    } catch (Exception e) {

                    }
                } else if (dataSnapshot.getKey().equals("l")) {
                    try {
                        if (loginSP.getBoolean(Global.IS_LOGGED_IN, false)
                                && dataSnapshot != null && dataSnapshot.getValue() != null
                                && !dataSnapshot.getValue().toString().equalsIgnoreCase("")) {
                            String version = CommonMethods.getVerisonNumber(StudentActivity.this);
                            HashMap<String, Object> hashMap = new HashMap<String, Object>();
                            if (dataSnapshot.getValue().toString().equalsIgnoreCase("ver"))
                                hashMap.put(dataSnapshot.getValue().toString(), version);
                            if (dataSnapshot.getValue().toString().equalsIgnoreCase("now"))
                                hashMap.put("dt", Calendar.getInstance().getTimeInMillis());
                            deviceRef.updateChildren(hashMap);
                            deviceRef.child("l").setValue("");
                        }
                    } catch (Exception e) {

                    }
                } else if (dataSnapshot.getKey().equals("nm")) {
                    loginSP.edit().putString(Global.DEVICE_NAME, dataSnapshot.getValue(String.class)).apply();
                    tv_deviceName.setText(dataSnapshot.getValue(String.class));
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        deviceRef.addChildEventListener(deviceRefListener);

        bizRef = PersistentDatabase.getDatabase().getReference("biz/"
                + loginSP.getString(Global.BIZ_ID, ""));

        if (bizRefListener != null) {
            bizRef.removeEventListener(bizRefListener);
        }

        bizRefListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getKey().equals("img")) {
                    loginSP.edit().putString(Global.BIZ_COMPANY_IMG, dataSnapshot.getValue(String.class)).apply();
            //        imageLoaderOriginal.DisplayImage(dataSnapshot.getValue(String.class), iv_businessLogo);
//                    Glide.with(getApplicationContext()).load(dataSnapshot.getValue(String.class)).into(iv_businessLogo);

                    Glide.with(getApplicationContext()).load(dataSnapshot.getValue(String.class)).asBitmap().override(200,200).fitCenter().diskCacheStrategy(DiskCacheStrategy.RESULT).into(new BitmapImageViewTarget(iv_businessLogo) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                iv_businessLogo.setImageDrawable(circularBitmapDrawable);
                            }
                        });



                    } else if (dataSnapshot.getKey().equals("nm")) {
                    loginSP.edit().putString(Global.BIZ_COMPANY_NAME, dataSnapshot.getValue(String.class)).apply();
                    tv_businessName.setText(dataSnapshot.getValue(String.class));
                } else if (dataSnapshot.getKey().equals("grp")) {
                    String groupName = dataSnapshot.child(loginSP.getString(Global.GROUP_ID, ""))
                            .child("nm").getValue(String.class);
                    loginSP.edit().putString(Global.GROUP_NAME,
                            groupName).apply();

                    tv_groupName.setText(groupName);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        bizRef.addChildEventListener(bizRefListener);


         /*To get the details of Mifare Sector Number...*/
        DatabaseReference nfcRef = PersistentDatabase.getDatabase().getReference("masterData")
                .child(loginSP.getString(Global.BIZ_ID,""))
                .child(loginSP.getString(Global.GROUP_ID,"")).child("nfc").child("mifare").child("id");

        nfcRef.keepSynced(true);

        nfcRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("sec")){
                    loginSP.edit().putInt(Global.MIFARE_SEC_NUM,
                            Integer.valueOf(dataSnapshot.child("sec").getValue().toString())).apply();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void reSendImagesToStorage(SharedPreferences imgRef) {
        ArrayList<String> imgList = new ArrayList<>();
        Map<String, String> mapData = (HashMap<String, String>) imgRef.getAll();

//        Log.v("DataStudent","Size"+mapData.toString()+"Size");
        for (Map.Entry<String, String> map : mapData.entrySet()) {
            String[] data = map.getKey().split("#");
            String student_Key = data[0];
            String key = data[1];
            String img = map.getValue();
            // imgList.add(img);
//            Log.v("DataStudent",mob);
//            Log.v("DataStudent",img);

            CommonMethods.uploadImages(StudentActivity.this, null, img, bizId, grpId, key, student_Key, false, isTypeIsSchool);
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.SEND_SMS)
                    == PackageManager.PERMISSION_GRANTED
                    ) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE",
                        "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA",
                        "android.permission.SEND_SMS"}, 1);
                return false;
            }
        } else {

            return true;
        }
    }

    private void getSettingValues() {
        DatabaseReference reference = PersistentDatabase.getDatabase().getReference("masterData/" + bizId + "/" + grpId + "/student/");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                SharedPreferences.Editor editor = spForSettings.edit();
                if (dataSnapshot.hasChild("search"))
                    editor.putBoolean(Global.SEARCH, dataSnapshot.child("searchMob").getValue(Boolean.class));
                if (dataSnapshot.hasChild("wd"))
                    editor.putFloat(Global.WEEKDAY, dataSnapshot.child("wd").getValue(Float.class));
                if (dataSnapshot.hasChild("we"))
                    editor.putFloat(Global.WEEKEND, dataSnapshot.child("we").getValue(Float.class));

                if (dataSnapshot.hasChild("notify") && dataSnapshot.child("notify").hasChild("in"))
                    editor.putBoolean(Global.IN_SETTINGS, dataSnapshot.child("notify").child("in").getValue(Boolean.class));

                if (dataSnapshot.hasChild("notify") && dataSnapshot.child("notify").hasChild("out"))
                    editor.putBoolean(Global.OUT_SETTINGS, dataSnapshot.child("notify").child("out").getValue(Boolean.class));
                if (dataSnapshot.hasChild("notify") && dataSnapshot.child("notify").hasChild("late"))
                    editor.putBoolean(Global.LATE_SETTING, dataSnapshot.child("notify").child("late").getValue(Boolean.class));

                if (dataSnapshot.hasChild("notify") && dataSnapshot.child("notify").hasChild("sendAll"))
                    editor.putBoolean(Global.SENDALL_SETTING, dataSnapshot.child("notify").child("sendAll").getValue(Boolean.class));


                if (dataSnapshot.hasChild("photo"))
                    editor.putBoolean(Global.PHOTO_MANDATORY, dataSnapshot.child("photo").getValue(Boolean.class));

                editor.apply();
                if (dataSnapshot.hasChild("hr")) {
                    try {
                        editor.putInt(Global.TIME_GAP, dataSnapshot.child("hr").getValue(Integer.class)).apply();
                    } catch (Exception e) {

                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void initGUI() {

        isStoragePermissionGranted();

        //imageLoaderOriginal = new ImageLoaderOriginal(this);
        initNavigationView();
        getFirebaseData();


        titles = new ArrayList<>();
        getSupportActionBar().setTitle("HOSTEL TRACKING");
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        pager = (ViewPager) findViewById(R.id.viewPager);
        pager.setOffscreenPageLimit(2);

        if (loginSP.getString(Global.TYP, "").equals("school")) {
            titles.add("Student");
        } else {
            titles.add("Staff");
        }
        titles.add("Out");
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), titles, num_of_tabs);
        pager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(pager);
        frag = (StudentEntryFragment) viewPagerAdapter.getItem(0);

    }

    private void initNavigationView() {

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);


        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        View headerView = navigationView.getHeaderView(0);
        tv_ver_Dtl = (TextView) findViewById(R.id.tv_ver_Dtl);
        tv_businessName = (TextView) headerView.findViewById(R.id.nav_header_businessName);
        tv_groupName = (TextView) headerView.findViewById(R.id.nav__header_groupName);
        iv_businessLogo = (ImageView) headerView.findViewById(R.id.nav_header_image);
        tv_deviceName = (TextView) headerView.findViewById(R.id.nav__header_deviceName);

        tv_businessName.setText(loginSP.getString(Global.BIZ_COMPANY_NAME, ""));
        tv_groupName.setText(loginSP.getString(Global.GROUP_NAME, ""));
        tv_deviceName.setText(loginSP.getString(Global.DEVICE_NAME, ""));

        PackageManager manager = getApplication().getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    getApplication().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = info.versionName;

        tv_ver_Dtl.setText("Ver " + version);

        if (!loginSP.getString(Global.BIZ_COMPANY_IMG, "").isEmpty()) {
//            imageLoaderOriginal.DisplayImage(loginSP.getString(Global.BIZ_COMPANY_IMG, ""), iv_businessLogo);
            Glide.with(this).load(loginSP.getString(Global.BIZ_COMPANY_IMG, "")).into(iv_businessLogo);
        } else {
            iv_businessLogo.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.app_icon));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
        try {
            IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanningResult != null) {
                if (scanningResult.getContents() != null) {
                    scanContent = scanningResult.getContents();
                    int type = Global.PASS_SCAN_QR_ENTRY;
                    frag.upDate(scanContent, type);
                    //  Log.v("TAG", "Scaner" + scanContent);
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "No data received!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            } else {
           /* Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();*/
                super.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
            FirebaseCrash.report(e);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //if we want to read the data inside a NFC  tag we can use **\ onNewIntent /** :
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (nfcReaderWriter.isNFCAvailable()) {
            nfcReaderWriter.readNFC(intent, this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(nfcReaderWriter.isNFCAvailable()){
            nfcReaderWriter.stopForegroundDispatch(StudentActivity.this,nfcReaderWriter.getNfcAdapter());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(nfcReaderWriter.isNFCAvailable()){
            nfcReaderWriter.setupForegroundDispatch(StudentActivity.this);
        }
    }

    @Override
    public void readNFCData(String tagData) {
        if (tagData != null) {
            StudentEntryFragment frag = (StudentEntryFragment) viewPagerAdapter.getItem(0);
            int type = Global.PASS_SCAN_NFC_ENTRY;
            frag.upDate(tagData, type);
        }
    }
}
