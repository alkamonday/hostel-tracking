package in.mondays.hostel.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import in.mondays.hostel.R;
import in.mondays.hostel.Utils.Global;

public class SplashScreenActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        sp = getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        getSupportActionBar().hide();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {



                if (sp.getBoolean(Global.IS_LOGGED_IN, false)) // this means user has already verified the mobile number
                {
                        Intent in = new Intent(getApplicationContext(), StudentActivity.class);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(in);
                     //   overridePendingTransition(0, 0);



                } else{
                    Intent in = new Intent(getApplicationContext(), VerificationActivity.class);
                    in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(in);
                }

            }
        }, 3000);



        initGUI();
    }

    private void initGUI() {
        progressBar = (ProgressBar) findViewById(R.id.pb_splashScreen);

    }
}
