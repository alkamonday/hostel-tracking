package in.mondays.hostel.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.util.ArrayList;

import in.mondays.hostel.GetterSetter.Student;

import in.mondays.hostel.R;
import in.mondays.hostel.Utils.CommonMethods;
import in.mondays.hostel.Utils.Global;

import static android.content.Context.MODE_PRIVATE;

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.MyViewHolder> {
    ArrayList<Student> studentArrayList;
    Context context;
    //    ImageLoaderOriginal imageLoaderOriginal;
    SharedPreferences imageSp;
    SharedPreferences loginsp;



    public StudentListAdapter(ArrayList<Student> studentArrayList, Context context) {
        this.studentArrayList = studentArrayList;
        this.context = context;
        imageSp=context.getSharedPreferences(Global.IMAGE_SP,Context.MODE_PRIVATE);
        loginsp = context.getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
    }




    public static class MyViewHolder extends RecyclerView.ViewHolder  {
        TextView stu_Name, stu_Mobile,stu_OutTime,stu_RollNumber,tv_Time_Difference;
        ImageView stu_Image;
        CardView selectCardview;
        //  ClickListner listner;
        View view;
        public MyViewHolder(View itemView) {
            super(itemView);
            view=itemView;
            //selectCardview= (CardView) itemView.findViewById(R.id.outList_CardView);
            stu_Name= (TextView) itemView.findViewById(R.id.tv_adapter_student_name);
            tv_Time_Difference= (TextView) itemView.findViewById(R.id.tv_Time_Difference);
            // stu_Mobile = (TextView) itemView.findViewById(R.id.tv_adapter_Student_mobile);
            //stu_RollNumber= (TextView) itemView.findViewById(R.id.tv_adapter_Student_RollNumber);
            stu_Image= (ImageView) itemView.findViewById(R.id.img_adapter_student);
            stu_OutTime= (TextView) itemView.findViewById(R.id.tv_adapter_Student_Out);


        }




    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.stu_history_list_items,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Student model= studentArrayList.get(position);
        holder.stu_Name.setText(model.getNm());
        //   holder.stu_Mobile.setText(model.getMob());
        // holder.stu_RollNumber.setText(model.getRollNo());
        if (model.getOut()!=null) {
            holder.stu_OutTime.setText(CommonMethods.getDate(model.getOut(), "dd MMM - hh:mm a"));
            holder.tv_Time_Difference.setText("");
            holder.tv_Time_Difference.setText(getTimeDifference(model.getOut()));

        }


/*
        if (loginsp.getString(Global.TYP,"").equals("school")){
            holder.stu_RollNumber.setText(model.getRollNo());
        }else {
            holder.stu_RollNumber.setText(model.getEmpId());
        }*/

        if(studentArrayList.get(position).getImg()!=null){
            // this class use to take the image from url
//            imageLoaderOriginal.DisplayImage(studentArrayList.get(position).getImg(), holder.stu_Image);
//            Glide.with(context)
//                    .load(studentArrayList.get(position).getImg())
//                    .override(200, 200)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(holder.stu_Image);
            Glide.with(context)
                    .load(studentArrayList.get(position).getImg())
                    .asBitmap().centerCrop()
                    .override(200, 200)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(new BitmapImageViewTarget(holder.stu_Image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            holder.stu_Image.setImageDrawable(circularBitmapDrawable);
                        }
                    });

        }else if( !TextUtils.isEmpty(imageSp.
                getString(studentArrayList.get(position)
                        .getId()+"#"+studentArrayList.get(position).gethKey(),""))){

            holder.stu_Image.setImageBitmap(new CommonMethods()
                    .decodeSampledBitmapFromFile(imageSp.getString(studentArrayList.
                            get(position).getId()+"#"+studentArrayList.get(position).gethKey(),""),300,300));

        }else{
            holder.stu_Image.setImageResource(R.drawable.default_user_picture);
        }
    }

    private String getTimeDifference(Long out) {
        long current_Time = System.currentTimeMillis();
        long dif=current_Time-out;
        long diffDays = dif / (24 * 60 * 60 * 1000);
        long diffHours = dif / (60 * 60 * 1000) % 24;
//        Log.v("diffDays",diffDays+"==="+diffHours);
        int time_Out= 0;
        //  Log.v("diffDays",  diffDays*24+diffHours +"  Total Time");
        Log.v("diffDays",  diffDays+"  Total Time");
        if (diffDays!=0){
            time_Out= (int) diffDays;
        }else{
            time_Out= (int) (diffDays*24+diffHours);

        }

//        final String Out_Date=CommonMethods.getDate(out,"dd");
//        final String Out_Time=CommonMethods.getDate(out,"HH");
//        final int Out_time_diff = Integer.parseInt((Out_Time));
//        final int out_date_diffe=Integer.parseInt(Out_Date);
//
//        final String current_Date = CommonMethods.getDate(current_Time, "dd");
//        final String currentTime = CommonMethods.getDate(current_Time, "HH");
//        final int current_time_diff = Integer.parseInt((currentTime));
//        final int current_date_diff=Integer.parseInt(current_Date);
//
//        int date_Diff=current_date_diff-out_date_diffe;
//        Log.v("getTimeDifference", current_date_diff+"  -  "+out_date_diffe+ " = "+date_Diff+"   Diff");
//
//        int diff=0;
//        if (date_Diff==0){
//            diff =  (Out_time_diff-current_time_diff);
//        }else {
//            int tDeff=(Out_time_diff-current_time_diff);
//            diff=((date_Diff-1)*24)+tDeff;
//        }


        if (diffDays!=0){
            if (diffDays==1) {
                return String.valueOf("Yesterday");
            }else {
                return String.valueOf(Math.abs(time_Out) + " Days");
            }
        }else {
            return String.valueOf(Math.abs(time_Out)+" hr");
        }
//        return String.valueOf(Math.abs(time_Out)+"day");
    }


    @Override
    public int getItemCount() {
        return studentArrayList.size();
    }



}
