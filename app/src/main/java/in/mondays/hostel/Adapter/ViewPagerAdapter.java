package in.mondays.hostel.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import in.mondays.hostel.Fragment.OutListFragment;
import in.mondays.hostel.Fragment.StudentEntryFragment;

/**
 * Created by developer on 7/12/16.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    ArrayList<String> Titles;
    int tabCounts;
    Fragment tab1,tab2;


    public ViewPagerAdapter(FragmentManager fm, ArrayList<String> titles, int tabCounts
                            ) {
        super(fm);
        Titles = titles;
        this.tabCounts = tabCounts;
        tab1=new StudentEntryFragment();
        tab2=new OutListFragment();

    }

    @Override
    public Fragment getItem(int position) {
        if (position==0){
            return tab1;
        }
        if (position==1){
            return tab2;
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabCounts;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles.get(position).toString();
    }
    @Override
    public int getItemPosition(Object object) {

        return super.getItemPosition(object);
    }

}
