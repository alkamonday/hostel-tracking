package in.mondays.hostel.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import in.mondays.hostel.Firebase.PersistentDatabase;


/**
 * Created by developer on 24/11/16.
 */

public class CommonMethods {

    public static String getDate(long milliSeconds, String dateFormat) {

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
    public static void writeToLogFile(String log) {


        try {
            File root = new File(Environment.getExternalStorageDirectory(), "HostelTracker");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, "log");
            FileWriter writer = new FileWriter(gpxfile, true);
            writer.append("dt: " + Calendar.getInstance().getTime() + "\n" + log + "\n");
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static boolean isInternetWorking(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static String getVerisonNumber(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo.versionName;
    }

    public static Toast showToast(Context context,String msg, int time)
    {
        Toast toast=Toast.makeText(context,msg,time);
        toast.setGravity(Gravity.CENTER,0,0);
        return toast;
    }
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static byte[] getBitmapAsByteArray(Bitmap bitmap, String sourcePath, String imageKey, Context context) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        if (sourcePath != null) {
            try {
                bitmap = rotateBitmap(sourcePath, bitmap, imageKey, context);
            } catch (Exception e) {
               /* SharedPreferences imgPathSharedPref=
                        context.getSharedPreferences(Global.VIZ_IMAGE_SP,Context.MODE_PRIVATE);
                if(imageKey!=null && imgPathSharedPref.contains(imageKey)) {
                    try {
                        File WhoCameDirectory = new File(imgPathSharedPref.getString(imageKey, ""));

                        if (WhoCameDirectory.exists())
                            WhoCameDirectory.delete();

                    }
                    catch (Exception ex)
                    {
                        CommonMethods.writeToLogFile("13 "+ex.toString());
                    }
                    finally {
                        imgPathSharedPref.edit().remove(imageKey).apply();
                    }
                }*/


                CommonMethods.writeToLogFile("12 " + e.toString());
            }
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, outputStream);

        return outputStream.toByteArray();
    }


    public static Bitmap rotateBitmap(String sourcepath, Bitmap bitmap, String imageKey, Context context) {
        int rotate = 0;

        try {
            File imageFile = new File(sourcepath);
            ExifInterface exif = new ExifInterface(
                    imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            //  e.printStackTrace();

           /* SharedPreferences imgPathSharedPref=
                    context.getSharedPreferences(Global.VIZ_IMAGE_SP,Context.MODE_PRIVATE);
            if(imageKey!=null && imgPathSharedPref.contains(imageKey)) {
                try {
                    File WhoCameDirectory = new File(imgPathSharedPref.getString(imageKey, ""));

                    if (WhoCameDirectory.exists())
                        WhoCameDirectory.delete();
                }catch (Exception ex)
                {
                    CommonMethods.writeToLogFile(ex.toString());
                }
                finally {
                    imgPathSharedPref.edit().remove(imageKey).apply();
                }

            }*/
            CommonMethods.writeToLogFile(e.toString());

        }
        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return bitmap;
    }

    public static void uploadImages(Context context, final String mob, String imgList,
                                    final String bizId, final String grpId,
                                    final String key, final String student_Key, final boolean updateSp,final boolean isTypeIsSchool) {
        String rootPath=null;
        if (isTypeIsSchool){
             rootPath="student";
        }else {
             rootPath="access";
        }


        final DatabaseReference matRef = PersistentDatabase.getDatabase().
                getReference(rootPath+"/"+bizId+"/"+grpId+"/"+ student_Key);

        final DatabaseReference hisRef = PersistentDatabase.getDatabase().
                getReference("hostelHistory/"+bizId+"/"+grpId+"/"+ key);


        final SharedPreferences imgRef = context.getSharedPreferences(Global.IMAGE_SP, Context.MODE_PRIVATE);

        // for (int i = 0; i < imgList.size(); i++) {
//            final int pos = i;
        try {
            if (updateSp)
            {
                imgRef.edit().putString(student_Key + "#" + key, imgList).apply();
            }
            StorageReference storageRef = FirebaseStorage.getInstance().getReference();
            StorageReference riversRef = storageRef.child(bizId+ "/"+grpId+"/"+rootPath+"/" + student_Key + ".jpg");
            UploadTask uploadTask = riversRef.putBytes(CommonMethods.getBitmapAsByteArray
                    (decodeSampledBitmapFromFile(imgList, 300, 400),
                            imgList, student_Key+"#"+key, context));
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    HashMap<String, Object> hashMap = new HashMap<String, Object>();
                    hashMap.put("img" , taskSnapshot.getDownloadUrl().toString());
                    matRef.updateChildren(hashMap);
                    hisRef.updateChildren(hashMap);
                    try {
                        File HostelDirectory = new File(imgRef.getString(student_Key+"#"+key, ""));
                        if (HostelDirectory.exists())
                            HostelDirectory.delete();
                    } catch (Exception e) {
                        CommonMethods.writeToLogFile("4 " + e.toString()); // writing to log file
                    } finally {
                        imgRef.edit().remove(student_Key + "#" + key).commit();
                        System.out.println("sp   "+imgRef.getAll().toString());
                    }
                }
            });
        } catch (Exception e) {
            if (imgRef.contains(student_Key)) {
                try {
                    File HostelDirectory = new File(imgRef.getString(student_Key+"#"+key, ""));
                    if (HostelDirectory.exists())
                        HostelDirectory.delete();
                } catch (Exception ex) {
                    CommonMethods.writeToLogFile("5 " + e.toString()); // writing to log file
                } finally {

                    imgRef.edit().remove(student_Key+"#"+key).apply();

                }
            }

            CommonMethods.writeToLogFile("6 " + e.toString()); // writing to log file
        }


        //}
    }

    public static boolean isTimeExceeded( long currentDt, long inDt, long timeLimit ) {

        Date cDt= new Date(currentDt);
        Date iDt= new Date(inDt);


        long  mills = cDt.getTime() - iDt.getTime();

        if (mills > (timeLimit)) {
            return true;
        } else {
            return false;
        }




    }
    public void logout(Context context) {

        SharedPreferences loginSp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        String uuid = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        DatabaseReference codeRef = PersistentDatabase.getDatabase().getReference("device/"+
                loginSp.getString(Global.BIZ_ID, "")+
                "/"+Global.APP_NAME+ "/" + uuid);

        codeRef.getRef().child("del").setValue(true);
        codeRef.getRef().child("dt").setValue(Calendar.getInstance().getTimeInMillis());
        SharedPreferences sp = context.getSharedPreferences(Global.LOGIN_SP, Context.MODE_PRIVATE);
        SharedPreferences.Editor editorlogin = sp.edit();
        editorlogin.clear();
        editorlogin.commit();

        //FirebaseAuth.getInstance().signOut();
        File listFile = new File(
                android.os.Environment.getExternalStorageDirectory()+File.separator+
                        "HostelTracking"+File.separator+"Images");
        if (listFile.exists())
            DeleteRecursive(listFile);
        deleteCache(context);
    }

    public static void DeleteRecursive(File fileOrDirectory)
    {
        if (fileOrDirectory.isDirectory())
        {
            for (File child : fileOrDirectory.listFiles())
            {
                DeleteRecursive(child);
            }
        }

        fileOrDirectory.delete();
    }

    public void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }
    public boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static String getCurrentTime(String s) {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }



}
