package in.mondays.hostel.Utils;

/**
 * Created by developer on 25/11/16.
 */

public class Global {

    public static final String SETTINGS = "settingForTimeCheck";

    public static final String WEEKDAY = "week_Day";
    public static final String WEEKEND = "week_End";
    public static final String OUT_SETTINGS = "out_Setting";
    public static final String IN_SETTINGS = "in_Setting";
    public static final String LATE_SETTING = "late_Setting";
    public static final String IMAGE_SP = "ImageSp";
    public static final String APP_NAME = "hostel";
    public static final String LOCATION_ID = "locId";
    public static final String SENDALL_SETTING = "sendAll_Setting";
    public static final String BIZ_COMPANY_NAME = "biz_company_name";
    public static final String DEVICE_NAME = "device_name";
    public static final String VERSION_EXPIRED = "ver_exep";
    public static final String SEARCH = "search";
    public static final String SCAN_SETTING = "scan_setting";
    public static final String TYP = "typ";

    public static String SERVER_ERROR_MSG = "Server error! please try after some time";
    public static final String LOGIN_SP="LoginSp_Hostel";
    public static final String BIZ_ID = "BizId";
    public static final String GROUP_ID = "GroupId";
    public static final String GROUP_NAME = "GroupNm";
    public static final String BIZ_EXPIRY_DATE="ExpDt";
    public static final String BIZ_COMPANY_IMG="CompanyImg";
    public static String NO_INTERNET_MSG = "No Internet Connection! Try again";
    public static final String DEVICE_ID="Device_Id";
    public static final String IS_LOGGED_IN="IsLoggedIn";
    public static final String VERSION_NUMBER="Version_No";
    public static final String BIZ_PERSON_NAME="biz_per_nm";
    public static final String TIME_GAP="timeGap";

    public static final int PASS_SCAN_NFC_ENTRY=4;
    public static final int PASS_SCAN_QR_ENTRY=1;
    public static final int MANNUAL_ENTRY=2;
    private static final int IN_ENTRY_MISSED = 3;

    public static final String PHOTO_MANDATORY = "photo_mandatory";

    public static final String MIFARE_SEC_NUM = "mifare_sec_num";
}
