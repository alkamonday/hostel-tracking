package in.mondays.hostel.GetterSetter;

/**
 * Created by developer on 7/12/16.
 */

public class Student  {
    String cls;
    String gndr;
    String img;
    String nm;
    String pNm;
    String pMob;
    String rmNo;
    String rollNo;
    String pur;
    String mob;
    Long in,out;
    String hKey;
    String id;
    String empId;

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    boolean msg;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    boolean isSelected;


    public boolean getMsg() {
        return msg;
    }

    public void setMsg(boolean msg) {
        this.msg = msg;
    }

    public String gethKey() {
        return hKey;
    }

    public void sethKey(String hKey) {
        this.hKey = hKey;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }


    public Long getIn() {
        return in;
    }

    public void setIn(Long in) {
        this.in = in;
    }

    public Long getOut() {

        return out;
    }

    public void setOut(Long out) {
        this.out = out;
    }

    public String getPur() {
        return pur;
    }

    public void setPur(String pur) {
        this.pur = pur;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public String getGndr() {
        return gndr;
    }

    public void setGndr(String gndr) {
        this.gndr = gndr;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }

    public String getpNm() {
        return pNm;
    }

    public void setpNm(String pNm) {
        this.pNm = pNm;
    }

    public String getpMob() {
        return pMob;
    }

    public void setpMob(String pMob) {
        this.pMob = pMob;
    }

    public String getRmNo() {
        return rmNo;
    }

    public void setRmNo(String rmNo) {
        this.rmNo = rmNo;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public Student() {

    }
}
