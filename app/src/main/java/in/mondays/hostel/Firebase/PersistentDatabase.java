package in.mondays.hostel.Firebase;

import com.google.firebase.database.FirebaseDatabase;


public class PersistentDatabase {

    private static FirebaseDatabase mDatabase;

    public static FirebaseDatabase getDatabase() {
        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
            mDatabase.setPersistenceEnabled(true);
        }
        return mDatabase;
    }

}
