package in.mondays.hostel.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import in.mondays.hostel.Adapter.StudentListAdapter;
import in.mondays.hostel.Firebase.PersistentDatabase;
import in.mondays.hostel.GetterSetter.Student;
import in.mondays.hostel.R;
import in.mondays.hostel.Utils.CommonMethods;
import in.mondays.hostel.Utils.Global;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by developer on 7/12/16.
 */

public class OutListFragment extends Fragment {
    private static final String TAG = "OutList";
    ArrayList<Student> studentArrayList;
    RecyclerView rv_Student_list;
    StudentListAdapter adapter;
    DatabaseReference dRef;
    boolean flag_StudentIsLate = false;
    float weak_End_Time, weak_Day_Time;
    boolean dateIsPrevious = false;
    boolean isLateMessageEnable = false;
    SharedPreferences spForSettings;
    boolean isStudent_Late = false;
    SharedPreferences loginsp;
    String bizId, grpId;
    Query query;
    private boolean isSenttoAll = false;
    private boolean isTypeIsSchool=false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_student_out_list, container, false);
        loginsp = getActivity().getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        spForSettings = getActivity().getSharedPreferences(Global.SETTINGS, MODE_PRIVATE);
        bizId = loginsp.getString(Global.BIZ_ID, "");
        grpId = loginsp.getString(Global.GROUP_ID, "");

        if (loginsp.getString(Global.TYP,"").equals("school")){
            isTypeIsSchool =true;
        }else {
            isTypeIsSchool =false;
        }
        init(rootView);
        return rootView;
    }

    private void init(View rootView) {
        studentArrayList = new ArrayList<>();
        rv_Student_list = (RecyclerView) rootView.findViewById(R.id.rv_Student_List_recyclerView);
        rv_Student_list.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new StudentListAdapter(studentArrayList, getActivity());
        adapter.notifyDataSetChanged();
        rv_Student_list.setAdapter(adapter);
        rv_Student_list.setLayoutManager(linearLayoutManager);
        final Context context = getActivity();



        DatabaseReference reference = PersistentDatabase.getDatabase().getReference("masterData/" + bizId + "/" + grpId + "/student/notify/" +
                "sendAll");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    spForSettings.edit().putBoolean(Global.SENDALL_SETTING, dataSnapshot.getValue(Boolean.class)).apply();
                    getActivity().invalidateOptionsMenu();
                } catch (Exception e) {
                    FirebaseCrash.report(e);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        dRef = PersistentDatabase.getDatabase().getReference().
                child("hostelHistory/" + bizId + "/" + grpId + "/");

        Query queryByI = dRef.orderByChild("i").equalTo(null);

        queryByI.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {


                    if (dataSnapshot.exists()) {
                        System.out.println("ChildAdded");
                        final Student student = dataSnapshot.getValue(Student.class);
                        student.sethKey(dataSnapshot.getKey());
                        String rootPath=null;
                        if (isTypeIsSchool){
                            rootPath="student";
                        }else {
                            rootPath="access";
                        }
                        DatabaseReference stuRef = PersistentDatabase.getDatabase()
                                .getReference(rootPath+"/" + bizId
                                        + "/" + grpId).child(student.getId());
                        stuRef.keepSynced(true);

                        stuRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot stuSnapShot) {
                                if (stuSnapShot.exists()) {
/*
                                        if (stuSnapShot.child("lbl").hasChild("hostel")) {
*/
                                    student.setNm(stuSnapShot.child("nm").getValue(String.class));
                                    student.setRollNo(stuSnapShot.child("rollNo").getValue(String.class));
                                    String parentKey = null;
                                    if (isTypeIsSchool){
                                        parentKey = stuSnapShot.child("pa").getValue(String.class);
                                        student.setRollNo(stuSnapShot.child("rollNo").getValue(String.class));
                                    }else {
                                        student.setEmpId(stuSnapShot.child("empId").getValue(String.class));
                                    }

                                    DatabaseReference parentMobRef = PersistentDatabase.
                                            getDatabase().getReference("parent/" + bizId + "/" + grpId + "/"
                                            + parentKey + "/mob/");

                                    parentMobRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot parentSnapshot) {
                                            student.setpMob(parentSnapshot.getValue(String.class));
//                                                    Log.v("parentKey    ",parentSnapshot.getValue(String.class));
                                            studentArrayList.add(student);
                                            adapter.notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });

                                      /*  }*/
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                }catch (Exception e){
                    FirebaseCrash.report(e);
                }

            }

            @Override
            public void onChildChanged(final DataSnapshot dataSnapshot, String s) {
                final Student student = dataSnapshot.getValue(Student.class);
                student.sethKey(dataSnapshot.getKey());
                String rootPath=null;
                if (isTypeIsSchool){
                    rootPath="student";
                }else {
                    rootPath="access";
                }

                DatabaseReference stuRef = PersistentDatabase.getDatabase()
                        .getReference(rootPath+"/" + bizId + "/" + grpId).child(student.getId());
                stuRef.keepSynced(true);
                stuRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot stuSnapShot) {
                        student.setNm(stuSnapShot.child("nm").getValue(String.class));



                        String parentKey = null;
                        if (isTypeIsSchool){
                            parentKey = stuSnapShot.child("pa").getValue(String.class);
                            student.setRollNo(stuSnapShot.child("rollNo").getValue(String.class));
                        }else {
                            student.setEmpId(stuSnapShot.child("empId").getValue(String.class));
                        }

                        DatabaseReference parentMobRef = PersistentDatabase.
                                getDatabase().getReference("parent/" + bizId + "/" + grpId + "/"
                                + parentKey + "/mob");
                        parentMobRef.keepSynced(true);
                        parentMobRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot parentSnapshot) {

                                student.setpMob(parentSnapshot.getValue(String.class));
//
                                Iterator<Student> ite = studentArrayList.iterator();
                                int count = 0;
                                while (ite.hasNext()) {
                                    Student student1 = ite.next();

                                    if (student1.gethKey().equals(dataSnapshot.getKey()) && dataSnapshot.hasChild("img")) {
                                        studentArrayList.set(count, student);
                                        adapter.notifyDataSetChanged();
                                    }
                                    count++;
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                System.out.println("onChildRemoved");
                Iterator<Student> ite = studentArrayList.iterator();
                while (ite.hasNext()) {
                    Student student1 = ite.next();
                    if (student1.gethKey().equals(dataSnapshot.getKey())) {
                        ite.remove();
                        adapter.notifyDataSetChanged();
                    }

                }


            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.send_message, menu);

        MenuItem item = menu.findItem(R.id.menu_late_msg);
        if (isTypeIsSchool) {
            item.setVisible(spForSettings.getBoolean(Global.SENDALL_SETTING, false));
        }else {
            item.setVisible(false);
        }
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.menu_late_msg:

                //   Toast.makeText(getContext(),"Menu Clicked",Toast.LENGTH_SHORT).show();
                if (studentArrayList.size() > 0) {

                    final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Are you sure?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            checkIsStudentLate();
                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            builder.setCancelable(true);
                        }
                    });
                    builder.show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    private void checkIsStudentLate() {
        Student model = new Student();
        long current_Time = System.currentTimeMillis();
        final String current_Date = CommonMethods.getDate(current_Time, "dd-MM-yyyy");
        final String current_Day = CommonMethods.getDate(current_Time, "EEE");
        final String currentTime_Student = CommonMethods.getDate(current_Time, "HH.mm");
        final float c_time = Float.parseFloat((currentTime_Student));

        weak_Day_Time = spForSettings.getFloat(Global.WEEKDAY, 0);
        weak_End_Time = spForSettings.getFloat(Global.WEEKEND, 0);
        isLateMessageEnable = spForSettings.getBoolean(Global.LATE_SETTING, false);
//        Log.v(TAG, "WD" + weak_Day_Time);
//        Log.v(TAG, "WD" + weak_End_Time);
        //On WEEK END

        for (int i = 0; i < studentArrayList.size(); i++) {
            model = studentArrayList.get(i);
//            if (model.isSelected()) {
            String hisKeys = studentArrayList.get(i).gethKey();
            String out_Date = CommonMethods.getDate(studentArrayList.get(i).getOut(), "dd-MM-yyyy");
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            try {
                Date outDate_Student = sdf.parse(out_Date);
                Date currentDat = sdf.parse(current_Date);
                if ((current_Day.trim().equalsIgnoreCase("Sat") || current_Day.trim().equalsIgnoreCase("Sun")
                        && (spForSettings.getFloat(Global.WEEKEND, 0) < c_time))
                        ||
                        (!current_Day.trim().equalsIgnoreCase("Sat") && !current_Day.trim().equalsIgnoreCase("Sun")
                                && (spForSettings.getFloat(Global.WEEKDAY, 0) < c_time))
                        ||
                        (currentDat.after(outDate_Student))
                        ) {
                    if (isLateMessageEnable && !studentArrayList.get(i).getMsg()) {//TO DO :-Check the Student is Exceeded to 48 hr....then sent the message to parent.

                        String outDate = CommonMethods.getDate(studentArrayList.get(i).getOut(), "dd-MM-yyyy");
//                        Log.v(TAG, "Loop" + outDate + "  ==  " + current_Date);
                        String msg = "Your child has not yet arrived in the hostel";
                        String parentMob = studentArrayList.get(i).getpMob();
                        SendMessageTOParents(parentMob, msg, hisKeys);
//                        Log.v(TAG, "Loop" + studentArrayList.get(i).getpMob());
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
    //  }

    private void SendMessageTOParents(String mob, String msg, String hisKeys) {
        if (!mob.equals("") && (!mob.equals(null))) {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(mob, null, msg, null, null);
                DatabaseReference hisRef = PersistentDatabase.getDatabase().
                        getReference("hostelHistory/" + bizId + "/" + grpId + "/" + hisKeys);
                hisRef.child("msg").setValue(true);
                Toast.makeText(getContext(), "Message Sent to -" + mob,
                        Toast.LENGTH_LONG).show();
                //  Log.v(TAG,"Message Sent to -"+mob);
                flag_StudentIsLate = false;
                isStudent_Late = false;
            } catch (Exception e) {
                FirebaseCrash.report(e);
            }

        } else {
            Toast.makeText(getContext(), "Message Not Sent to" + mob, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    protected void onClickListner(View view) {
        if (view == rv_Student_list) {
            view.refreshDrawableState();
        }
    }

}