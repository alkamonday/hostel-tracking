package in.mondays.hostel.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.zxing.integration.android.IntentIntegrator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.mondays.hostel.Firebase.PersistentDatabase;
import in.mondays.hostel.GetterSetter.Student;

import in.mondays.hostel.R;
import in.mondays.hostel.Utils.CommonMethods;
import in.mondays.hostel.Utils.Global;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by developer on 7/12/16.
 */

public class StudentEntryFragment extends Fragment implements View.OnClickListener, TextWatcher {

    private static final int REQUEST_CAMERA = 1000;
    private static final int IN_ENTRY_MISSED = 3;
    String TAG = "StudentE";
    LinearLayout ll_Student_Info, ll_purpose, ll_Roll_Number_btn;
    EditText et_Student_Search, et_Purpose;
    Button btn_Scan, btn_Submit;
    ImageView btn_Search_Roll_Number, img_Clear_Data;
    TextView tv_sName, tv_sClass, tv_sRoomNumber, tv_late_Reason, btn_update;
    ImageView stu_Image;
    //ImageLoaderOriginal imageLoaderOriginal;
    String s_Img_Url, parent_mob, student_Key;
    File photoPath;
    //Verification type for Cheking verification is Manual Or By Card
    int verificationType = Global.MANNUAL_ENTRY;

    boolean hisRefIsPresent = false;
    boolean inIsChecked = false;
    boolean outIsChecked = false;
    boolean isTypeIsSchool = false;

    boolean imageIsPresent = false;
    String imgUrl = null;
    boolean isStudent_Come_Late = false;
    Query query;
    //TO Check Messaging is Enable Are Not
    SharedPreferences spForSettings;
    /*  boolean isInMessageEnable=false;
      boolean isOutMessageEnable=false;
      boolean isLateMessageEnable=false;*/
    float weak_End_Time, weak_Day_Time;

    Bitmap bmp = null;
    int type;
    String in_HisRef = "";
    String parentMob;
    Bitmap resizeBitmapImage;
    byte[] imagePhoto;
    ArrayList<String> imgList;
    StorageReference firebaseStorage;
    SharedPreferences loginsp;
    String bizId, grpId;
    String sNumber;
    DatabaseReference timeGapRef;
    String his_key;
    SharedPreferences imgRef;
    DatabaseReference adminDb;
    ValueEventListener adminListener;
    LinearLayout ll_for_ver_exp;
    private boolean isVerificationByScan = false;
    private String student_Mob;
    private boolean isVerificationByRollNumber = false;

    public static void deleteLastImgCaptured(Context context) {

        String[] projection = {MediaStore.Images.Media.DATA, BaseColumns._ID};
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection, null, null, null);
        try {
            int column_index_data = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToLast();

            // String imagePath = cursor.getString(column_index_data);


            context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    BaseColumns._ID + "=" + cursor.getString(1), null);
        } catch (Exception e) {
            FirebaseCrash.report(e);

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_student_entry, container, false);
        firebaseStorage = FirebaseStorage.getInstance().getReference();
        //imageLoaderOriginal = new ImageLoaderOriginal(getActivity());
        loginsp = getActivity().getSharedPreferences(Global.LOGIN_SP, MODE_PRIVATE);
        bizId = loginsp.getString(Global.BIZ_ID, "");
        grpId = loginsp.getString(Global.GROUP_ID, "");

        if (loginsp.getString(Global.TYP, "").equals("school")) {
            isTypeIsSchool = true;
        } else {
            isTypeIsSchool = false;
        }

        DatabaseReference keepSynk = PersistentDatabase.getDatabase().
                getReference("student/" + bizId + "/" + grpId + "/");
        keepSynk.keepSynced(true);
        initView(rootView);
        checkVersion(getActivity());// setting the listners on Ui elements
        return rootView;
    }

    private void cheskSetting(View rootView) {
        DatabaseReference reference = PersistentDatabase.getDatabase().getReference();
    }

    private void checkVersion(final Context context) {
        adminDb = PersistentDatabase.getDatabase().getReference("admin/app/hostel");
        adminDb.keepSynced(true);
        adminListener = adminDb.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot child) {
                try {
                    PackageInfo pInfo = null;
                    try {
                        pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                    } catch (PackageManager.NameNotFoundException e) {
                        // e.printStackTrace();
                        FirebaseCrash.report(e);
                    }
                    String version = pInfo.versionName;

                    loginsp.edit().putString(Global.VERSION_NUMBER, version).apply();


                    if (!child.child("ver").getValue().toString().equalsIgnoreCase(version))

                    {


                        ll_for_ver_exp.setVisibility(View.VISIBLE);

                        if (System.currentTimeMillis() >= child.child("dt").getValue(Long.class)) {
                            btn_Scan.setVisibility(View.GONE);
                            btn_Submit.setVisibility(View.GONE);
                            loginsp.edit().putBoolean(Global.VERSION_EXPIRED, true).apply();

                        } else {
                            btn_Scan.setVisibility(View.VISIBLE);
                            btn_Submit.setVisibility(View.VISIBLE);
                            loginsp.edit().putBoolean(Global.VERSION_EXPIRED, false).apply();
                        }
                    } else {
                        ll_for_ver_exp.setVisibility(View.GONE);
                        btn_Scan.setVisibility(View.VISIBLE);
                        btn_Submit.setVisibility(View.VISIBLE);
                        loginsp.edit().putBoolean(Global.VERSION_EXPIRED, false).apply();
                    }
                } catch (Exception e) {
                    FirebaseCrash.report(e);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //Initialize the view
    private void initView(View rootView) {
        spForSettings = getActivity().getSharedPreferences(Global.SETTINGS, MODE_PRIVATE);
        DatabaseReference reference = PersistentDatabase.getDatabase().getReference("masterData/" + bizId + "/" + grpId
                + "/student/searchMob");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    spForSettings.edit().putBoolean(Global.SCAN_SETTING, dataSnapshot.getValue(Boolean.class)).apply();
                    if (!dataSnapshot.getValue(Boolean.class)) {
                        btn_Search_Roll_Number.setVisibility(View.VISIBLE);
                        et_Student_Search.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        et_Student_Search.setHint(isTypeIsSchool ? "Roll Number" : "Employee Id");
                        isVerificationByRollNumber = true;
                      /*  int maxLength = 18;
                        InputFilter[] FilterArray = new InputFilter[1];
                        FilterArray[0] = new InputFilter.LengthFilter(maxLength);
                        et_Student_Search.setFilters(FilterArray);*/
                    } else {
                        btn_Search_Roll_Number.setVisibility(View.GONE);
                        isVerificationByScan = false;
                        isVerificationByRollNumber = false;
                        et_Student_Search.setInputType(InputType.TYPE_CLASS_NUMBER);
                        et_Student_Search.setHint("Mobile");
                   /*     int maxLength = 10;
                        InputFilter[] FilterArray = new InputFilter[1];
                        FilterArray[0] = new InputFilter.LengthFilter(maxLength);
                        et_Student_Search.setFilters(FilterArray);*/

                    }

                } catch (Exception e) {
                    FirebaseCrash.report(e);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ll_Student_Info = (LinearLayout) rootView.findViewById(R.id.ll_Student_Info);
        ll_purpose = (LinearLayout) rootView.findViewById(R.id.ll_purpose);
        ll_for_ver_exp = (LinearLayout) rootView.findViewById(R.id.ll_version_expired);
        //ll_Roll_Number_btn= (LinearLayout) rootView.findViewById(R.id.ll_RollNumber_btn);


        et_Student_Search = (EditText) rootView.findViewById(R.id.et_Student_Search);
        et_Purpose = (EditText) rootView.findViewById(R.id.et_Purpose);
        et_Purpose.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);


        tv_sName = (TextView) rootView.findViewById(R.id.tv_Student_name);
//        tv_sClass= (TextView) rootView.findViewById(R.id.tv_Class_name);
        tv_sRoomNumber = (TextView) rootView.findViewById(R.id.tv_RoomNumber);
        tv_late_Reason = (TextView) rootView.findViewById(R.id.tv_purpose);

        btn_update = (TextView) rootView.findViewById(R.id.btn_ver_update);
        btn_Submit = (Button) rootView.findViewById(R.id.btn_Submit);

        img_Clear_Data = (ImageView) rootView.findViewById(R.id.img_clear_Data);
        stu_Image = (ImageView) rootView.findViewById(R.id.img_StudentImage);
        // stu_Take_Image= (ImageView) rootView.findViewById(R.id.stu_ImgTake);

        btn_Scan = (Button) rootView.findViewById(R.id.btn_Scan);
        btn_Search_Roll_Number = (ImageView) rootView.findViewById(R.id.btn_Roll_Number);
//        btn_Mobile_Number= (Button) rootView.findViewById(R.id.btn_Mobile_Number);

//
//        if (spForSettings.getBoolean(Global.SCAN_SETTING,false)){
//            if (isVerificationByRollNumber) {
//                btn_Search_Roll_Number.setVisibility(View.GONE);
//            }
//            et_Student_Search.setInputType(InputType.TYPE_CLASS_NUMBER);
//        //    et_Student_Search.addTextChangedListener(this);
//            img_Clear_Data.setEnabled(true);
//        }

        et_Student_Search.addTextChangedListener(this);

//        else{
////            ll_Roll_Number_btn.setVisibility(View.VISIBLE);
//            btn_Search_Roll_Number.setVisibility(View.VISIBLE);
//            img_Clear_Data.setOnClickListener(this);
//        }


        imgList = new ArrayList<>();
        imgRef = getActivity().getSharedPreferences(Global.IMAGE_SP, Context.MODE_PRIVATE);


        img_Clear_Data.setOnClickListener(this);
        btn_update.setOnClickListener(this);
        btn_Scan.setOnClickListener(this);
        btn_Search_Roll_Number.setOnClickListener(this);
        btn_Submit.setOnClickListener(this);
        stu_Image.setOnClickListener(this);


        timeGapRef = PersistentDatabase.getDatabase().
                getReference("masterData/" + bizId + "/" + grpId + "/student/timeGap");
        timeGapRef.keepSynced(true);


    }

    private void verifiesStudentRollNumber(final String student_number) {

        DatabaseReference vere_Ref = PersistentDatabase.getDatabase().
                getReference("student/" + bizId + "/" + grpId + "/");

        vere_Ref.keepSynced(true);

        if (isVerificationByRollNumber && isTypeIsSchool) {
            query = vere_Ref.orderByChild("rollNo").equalTo(student_number.toUpperCase());
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        final DataSnapshot snapshot = dataSnapshot.getChildren().iterator().next();
                        if (snapshot.child("lbl").hasChild("hostel")) {
                            btn_Search_Roll_Number.setEnabled(false);
                            student_Key = snapshot.getKey();
                            DatabaseReference parentMobRef = PersistentDatabase.
                                    getDatabase().getReference("parent/" + bizId + "/" + grpId + "/" + snapshot.child("pa").getValue());
                            parentMobRef.keepSynced(true);
                            parentMobRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    parent_mob = dataSnapshot.child("mob").getValue(String.class);
                                    if (snapshot.hasChild("his_Ref")) {
                                        in_HisRef = snapshot.child("his_Ref").getValue(String.class);
                                        hisRefIsPresent = true;
                                    }
                                    showDialogForEntryType(student_number, snapshot);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });
                        } else {
                            btn_Search_Roll_Number.setVisibility(View.VISIBLE);

                            ll_Student_Info.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                            // }
                        }
                    } else {
                        btn_Search_Roll_Number.setVisibility(View.VISIBLE);

                        Toast.makeText(getContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else if (isVerificationByRollNumber && !isTypeIsSchool) {
            DatabaseReference vere_Staff = PersistentDatabase.getDatabase().
                    getReference("access/" + bizId + "/" + grpId + "/");
            query = vere_Staff.orderByChild("empId").equalTo(student_number.toUpperCase());
            vere_Staff.keepSynced(true);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        btn_Search_Roll_Number.setVisibility(View.GONE);
                        final DataSnapshot snapshot = dataSnapshot.getChildren().iterator().next();
                        if (snapshot.child("lbl").hasChild("hostel")) {
                            student_Key = snapshot.getKey();
                            if (snapshot.hasChild("his_Ref")) {
                                in_HisRef = snapshot.child("his_Ref").getValue(String.class);
                                hisRefIsPresent = true;
                            }
                            showDialogForEntryType(student_number, snapshot);

                        } else {
                            btn_Search_Roll_Number.setVisibility(View.VISIBLE);
                            ll_Student_Info.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                            // }
                        }
                    } else {
                        btn_Search_Roll_Number.setVisibility(View.VISIBLE);
                        Toast.makeText(getContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

        if (!CommonMethods.isInternetWorking(getContext()) && isVerificationByRollNumber) {
            btn_Search_Roll_Number.setVisibility(View.VISIBLE);
        }


//      /*  if (isVerificationByScan){
//            query=vere_Ref.orderByChild("rollNo").equalTo(student_number);
//            isVerificationByScan=false;
//        }else {
//            query=vere_Ref.orderByChild("mob").equalTo(student_number);
//
//        }        query.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    final DataSnapshot snapshot=dataSnapshot.getChildren().iterator().next();
//                    if (snapshot.child("lbl").hasChild("hostel")) {
//                        student_Key=snapshot.getKey();
//                        DatabaseReference parentMobRef = PersistentDatabase.
//                                getDatabase().getReference("parent/" + bizId + "/" + grpId + "/" + snapshot.child("pa").getValue());
//                        parentMobRef.keepSynced(true);
//                        parentMobRef.addListenerForSingleValueEvent(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(DataSnapshot dataSnapshot) {
//                                parent_mob = dataSnapshot.child("mob").getValue(String.class);
//                                if (snapshot.hasChild("his_Ref")) {
//                                    in_HisRef = snapshot.child("his_Ref").getValue(String.class);
//                                    hisRefIsPresent = true;
//                                }
//                                showDialogForEntryType(student_number, snapshot);
//                            }
//                            @Override
//                            public void onCancelled(DatabaseError databaseError) {
//                            }
//                        });
//                    }else {
//                        ll_Student_Info.setVisibility(View.GONE);
//                        Toast.makeText(getContext(),"Record Not Found",Toast.LENGTH_SHORT).show();
//                        // }
//                    }
//                }
//            }
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });*/

    }

    private void searchByStaffMobileNumber(final String sNumber) {
        DatabaseReference vere_Reff = PersistentDatabase.getDatabase().
                getReference("access/" + bizId + "/" + grpId + "/");
        query = vere_Reff.orderByChild("mob").equalTo(sNumber);
        vere_Reff.keepSynced(true);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    final DataSnapshot snapshot = dataSnapshot.getChildren().iterator().next();
                    if (snapshot.child("lbl").hasChild("hostel")) {
                        student_Key = snapshot.getKey();
                        if (snapshot.hasChild("his_Ref")) {
                            in_HisRef = snapshot.child("his_Ref").getValue(String.class);
                            hisRefIsPresent = true;
                        }
                        showDialogForEntryType(sNumber, snapshot);
                    } else {
                        ll_Student_Info.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                        // }
                    }
                } else {
                    ll_Student_Info.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                    // }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void searchByMobileNumber(final String student_number) {
        DatabaseReference vere_Reff = PersistentDatabase.getDatabase().
                getReference("student/" + bizId + "/" + grpId + "/");
        query = vere_Reff.orderByChild("mob").equalTo(student_number);
        vere_Reff.keepSynced(true);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    final DataSnapshot snapshot = dataSnapshot.getChildren().iterator().next();
                    if (snapshot.child("lbl").hasChild("hostel")) {
                        student_Key = snapshot.getKey();
                        DatabaseReference parentMobRef = PersistentDatabase.
                                getDatabase().getReference("parent/" + bizId + "/" + grpId + "/" + snapshot.child("pa").getValue());
                        parentMobRef.keepSynced(true);
                        parentMobRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                parent_mob = dataSnapshot.child("mob").getValue(String.class);
                                if (snapshot.hasChild("his_Ref")) {
                                    in_HisRef = snapshot.child("his_Ref").getValue(String.class);
                                    hisRefIsPresent = true;
                                }
                                showDialogForEntryType(student_number, snapshot);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    } else {
                        ll_Student_Info.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                        // }
                    }
                } else {
                    ll_Student_Info.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                    // }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void searchByStudentRollNumber(final String rollNumber){

        DatabaseReference vere_Ref = PersistentDatabase.getDatabase().
                getReference("student/" + bizId + "/" + grpId + "/");

        vere_Ref.keepSynced(true);

        query = vere_Ref.orderByChild("rollNo").equalTo(rollNumber);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    final DataSnapshot snapshot = dataSnapshot.getChildren().iterator().next();
                    if (snapshot.child("lbl").hasChild("hostel")) {
                        btn_Search_Roll_Number.setEnabled(false);
                        student_Key = snapshot.getKey();
                        DatabaseReference parentMobRef = PersistentDatabase.
                                getDatabase().getReference("parent/" + bizId + "/" + grpId + "/" + snapshot.child("pa").getValue());
                        parentMobRef.keepSynced(true);
                        parentMobRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                parent_mob = dataSnapshot.child("mob").getValue(String.class);
                                if (snapshot.hasChild("his_Ref")) {
                                    in_HisRef = snapshot.child("his_Ref").getValue(String.class);
                                    hisRefIsPresent = true;
                                }
                                showDialogForEntryType(rollNumber, snapshot);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    } else {
                        btn_Search_Roll_Number.setVisibility(View.VISIBLE);
                        ll_Student_Info.setVisibility(View.GONE);

                        Toast.makeText(getContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                        // }
                    }
                } else {
                    btn_Search_Roll_Number.setVisibility(View.VISIBLE);

                    Toast.makeText(getContext(), "Record Not Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void showDialogForEntryType(final String sNumber, final DataSnapshot dataSnapshot) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.entry_type_dialog);
        dialog.setCancelable(false);
        RadioGroup rd_selection = (RadioGroup) dialog.findViewById(R.id.rd_entry_selection);
        final RadioButton rd_in_Entry = (RadioButton) dialog.findViewById(R.id.rd_In_entry);
        final RadioButton rd_out_Entry = (RadioButton) dialog.findViewById(R.id.rd_out_entry);
        final Button btn_Ok = (Button) dialog.findViewById(R.id.btn_dialog_Ok);
        final Button btn_Cancel = (Button) dialog.findViewById(R.id.btn_dialog_Cancel);
        final Student student = dataSnapshot.getValue(Student.class);


        btn_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_Student_Search.setText("");
                if (!spForSettings.getBoolean(Global.SCAN_SETTING, false)) {
                    btn_Search_Roll_Number.setEnabled(true);
                }
                if (isVerificationByRollNumber) {
                    isVerificationByScan = false;
                    btn_Search_Roll_Number.setVisibility(View.VISIBLE);
                }
                clearStudentForm();
                dialog.cancel();
            }
        });


        btn_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rd_in_Entry.isChecked()) {
                    ll_Student_Info.setVisibility(View.VISIBLE);
                    ll_purpose.setVisibility(View.GONE);
                    inIsChecked = true;
                    dialog.dismiss();
                } else if (rd_out_Entry.isChecked()) {
                    et_Purpose.setVisibility(View.VISIBLE);
                    ll_Student_Info.setVisibility(View.VISIBLE);
                    outIsChecked = true;
                    ll_purpose.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                } else {
                    Toast.makeText(getContext(), "Please Select Check Box.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        //CommonMethods.isInternetWorking(getContext())&&


//        Method for get previously update image

        Map<String, ?> keys = imgRef.getAll();
        int count = 0;
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            Log.v("GetAllImage", entry.toString());
            String[] split = entry.getKey().split("#");
            String student_Id = split[0];
            String his_key = split[1];
            if (student_Id.equals(student_Key)) {
                try {
                    stu_Image.setVisibility(View.VISIBLE);
                    imageIsPresent = true;
                    imgUrl = null;
                    photoPath = new File(imgRef.getString(entry.getKey(), null));
//                    stu_Image.setImageBitmap(CommonMethods.decodeSampledBitmapFromFile(imgRef.getString(entry.getKey(), null), 300, 400));
                    Glide.with(getContext()).load(imgRef.getString(entry.getKey(), null)).asBitmap().override(200,200).fitCenter().diskCacheStrategy(DiskCacheStrategy.RESULT).into(new BitmapImageViewTarget(stu_Image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            stu_Image.setImageDrawable(circularBitmapDrawable);
                        }
                    });

                    break;
                } catch (Exception e) {
                    FirebaseCrash.report(e);
                }
            }
        }
        if (photoPath == null) {
            if (student.getImg() != null) {
                //imageLoaderOriginal.DisplayImage(student.getImg(), stu_Image);
//                Glide.with(getActivity())
//                        .load(student.getImg())
//                        .override(200, 200)
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .into(stu_Image);
                Glide.with(getActivity()).load(student.getImg())
                        .asBitmap().centerCrop()
                        .override(200, 200)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(new BitmapImageViewTarget(stu_Image) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        stu_Image.setImageDrawable(circularBitmapDrawable);
                    }
                });
                imageIsPresent = true;
                imgUrl = student.getImg();
            } else {
                stu_Image.setVisibility(View.VISIBLE);
//                stu_Image.setImageResource(R.drawable.default_user_picture);

                Glide.with(getActivity()).load(R.drawable.default_user_picture)
                        .asBitmap().centerCrop()
                        .override(200, 200)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(new BitmapImageViewTarget(stu_Image) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                stu_Image.setImageDrawable(circularBitmapDrawable);
                            }
                        });


                imgUrl = null;
                imageIsPresent = false;
                // Toast.makeText(getContext(),"Image Not Present",Toast.LENGTH_SHORT).show();
                //  stu_Take_Image.setVisibility(View.VISIBLE);
            }
           /* else if(imgRef.getString(dataSnapshot.getKey()+"#"+in_HisRef,null)!=null)
            {
                stu_Image.setVisibility(View.VISIBLE);
                imageIsPresent=true;
                imgUrl=null;
                stu_Image.setImageBitmap(CommonMethods.decodeSampledBitmapFromFile(imgRef.getString(dataSnapshot.getKey()+"#"+in_HisRef,null),300,400));
            }*/

        }
        student_Mob = student.getMob();
        s_Img_Url = student.getImg();
        tv_sName.setText(student.getNm());
//        tv_sClass.setText(student.getCls());
        tv_sRoomNumber.setText(student.getRmNo());
        dialog.show();

    }

    private boolean isStudentLate() {
        long current_Time = System.currentTimeMillis();
        final String current_Day = CommonMethods.getDate(current_Time, "EEE");
        final String currentTime_Student = CommonMethods.getDate(current_Time, "HH.mm");
        final float c_time = Float.parseFloat((currentTime_Student));

          /*  isInMessageEnable=spForSettings.getBoolean(Global.IN_SETTINGS,false);
            isOutMessageEnable=spForSettings.getBoolean(Global.OUT_SETTINGS,false);
            isLateMessageEnable=spForSettings.getBoolean(Global.LATE_SETTING,false);*/
        if (current_Day.trim().equalsIgnoreCase("Sat") || current_Day.trim().equalsIgnoreCase("Sun")) {

            if (spForSettings.getFloat(Global.WEEKEND, 0) < c_time) {
                isStudent_Come_Late = true;
//                Log.v(TAG,"Late...."+String.valueOf(weak_End_Time+"-"+c_time+"=   Late"));
                return true;
            } else {
//                Log.v(TAG,"OnTime"+String.valueOf(weak_End_Time+"-"+c_time+"= OnTime"));
                return false;
            }
        } else {
            if (spForSettings.getFloat(Global.WEEKDAY, 0) < c_time) {
                isStudent_Come_Late = true;
//                Log.v(TAG,"Late"+String.valueOf(weak_Day_Time+"-"+c_time+"=   late"));
                return true;
            } else {
//                Log.v(TAG,"OnTime"+String.valueOf(weak_Day_Time+"-"+c_time+"= On time"));
                return false;
            }
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_Roll_Number:
                sNumber = et_Student_Search.getText().toString();
                if (!et_Student_Search.getText().toString().trim().equals("")) {
                    btn_Search_Roll_Number.setVisibility(View.GONE);
                    sNumber = et_Student_Search.getText().toString();
                    btn_Submit.setTag(type);
                    isVerificationByScan = true;
                    verifiesStudentRollNumber(sNumber);

                } else {
                    isVerificationByScan = false;
                    // et_Student_Search.setError("Can't empty");
                    et_Student_Search.setHintTextColor(Color.RED);
                }
                break;
//            case R.id.btn_Mobile_Number:
//                sNumber=et_Student_Search.getText().toString();
//                if(!et_Student_Search.getText().toString().trim().equals("")) {
//                    searchByMobileNumber(sNumber);
//                }
//                else
//                {
//                    et_Student_Search.setError("Can't empty");
//                }
//                break;

            case R.id.img_clear_Data:
                if (!spForSettings.getBoolean(Global.SCAN_SETTING, false)) {
                    btn_Search_Roll_Number.setEnabled(true);
                }
                if (isVerificationByRollNumber) {
                    isVerificationByScan = false;
                    btn_Search_Roll_Number.setVisibility(View.VISIBLE);
                }
                clearStudentForm();
                et_Student_Search.setText("");
                break;

            case R.id.btn_ver_update:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=in.mondays.hostel"));
                startActivity(intent);
                break;

            case R.id.img_StudentImage:

                Intent takeImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String photo = createFileFolder(student_Mob + ".jpg");
                photoPath = new File(photo);

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                    takeImage.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getContext(),
                            "in.mondays.hostel", photoPath));
                } else {
                    takeImage.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoPath));
                }

                if (takeImage.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivityForResult(takeImage, REQUEST_CAMERA);
                }

                break;

            case R.id.et_Student_Search:
                String student_Number = et_Student_Search.getText().toString();
                btn_Submit.setTag(student_Number);
                break;

            case R.id.btn_Scan:
//                Toast.makeText(getActivity(),"Verification",Toast.LENGTH_SHORT).show();
                if (!et_Student_Search.getText().toString().trim().equals(null)) {
                    clearStudentForm();
                }
                IntentIntegrator integrator = new IntentIntegrator(getActivity());
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setPrompt("Scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.initiateScan();
                break;

            case R.id.btn_Submit:
               /* String purpose=et_Purpose.getText().toString();
                //  isStudentLate(et_Student_Search.getText().toString());

                boolean valid[]=validate();

                if (!spForSettings.getBoolean(Global.SCAN_SETTING,false)){
                    btn_Search_Roll_Number.setEnabled(true);

                }

                if((valid[0] && outIsChecked && valid[1]) ||
                        (valid[1] && inIsChecked)&&isTypeIsSchool)
                {
                    if(inIsChecked &&  isStudentLate() && spForSettings.getBoolean(Global.LATE_SETTING,false))
                        SendMessageTOParents(parent_mob,"Your child has entered late in the hostel");
                    else
                    if(inIsChecked && spForSettings.getBoolean(Global.IN_SETTINGS,false))
                        SendMessageTOParents(parent_mob,"Your child has entered the hostel");
                    else
                    if(outIsChecked && spForSettings.getBoolean(Global.OUT_SETTINGS,false))
                    {
                        SendMessageTOParents(parent_mob,"Your child has left the hostel for "+purpose);
                    }
//                    Log.v(TAG,"Type"+verificationType);
                    btn_Submit.setEnabled(false);
                    saveStudentData(verificationType);
                    if (isVerificationByRollNumber){
                        isVerificationByScan=false;
                        btn_Search_Roll_Number.setVisibility(View.VISIBLE);
                    }

                }else {
                    if ((valid[0] && outIsChecked && valid[1]) ||
                            (valid[1] && inIsChecked)) {
                        Log.v("True","true");
                    }
                    saveStudentData(verificationType);
                }


                if (isVerificationByRollNumber){
                    btn_Search_Roll_Number.setVisibility(View.VISIBLE);
                }*/

                if (!validate()) {
                    return;
                }

                String purpose = et_Purpose.getText().toString().trim();

                if (!spForSettings.getBoolean(Global.SCAN_SETTING, false)) {
                    btn_Search_Roll_Number.setEnabled(true);
                }

                if (isTypeIsSchool) {
                    if (inIsChecked && isStudentLate() && spForSettings.getBoolean(Global.LATE_SETTING, false))
                        SendMessageTOParents(parent_mob, "Your child has entered late in the hostel");
                    else if (inIsChecked && spForSettings.getBoolean(Global.IN_SETTINGS, false))
                        SendMessageTOParents(parent_mob, "Your child has entered the hostel");
                    else if (outIsChecked && spForSettings.getBoolean(Global.OUT_SETTINGS, false)) {
                        SendMessageTOParents(parent_mob, !purpose.isEmpty() ? "Your child has left the hostel for " + purpose
                        : "Your child has left the hostel");
                    }
                }
                btn_Submit.setEnabled(false);
                saveStudentData(verificationType);
                if (isVerificationByRollNumber) {
                    isVerificationByScan = false;
                    btn_Search_Roll_Number.setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    public String createFileFolder(String filename) {
        File HostelDirectory = new File(Environment.getExternalStorageDirectory() +
                File.separator + "HostelTracker" + File.separator + "Images");

        if (!HostelDirectory.exists())
            HostelDirectory.mkdirs();

        File outputFile = new File(HostelDirectory, filename);

        return outputFile.getAbsolutePath();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CAMERA) {
            Bitmap rImage = null;
            if (resultCode == Activity.RESULT_OK) {
                try {
                    File studentImageFilefinal = new File(photoPath.getAbsolutePath());
                    Uri uri;

                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        uri = FileProvider.getUriForFile(getContext(),
                                "in.mondays.hostel", studentImageFilefinal);
                    } else {
                        uri = Uri.fromFile(studentImageFilefinal);
                    }

                    bmp = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    // rImage=CommonMethods.rotateBitmap(photoPath.toString(),bmp,null,null);

                } catch (Exception e) {

                    Toast.makeText(getActivity(), "Something went wrong! try again", Toast.LENGTH_SHORT).show();

                }

                if (bmp != null) {
                    bmp = Bitmap.createScaledBitmap(CommonMethods.rotateBitmap(photoPath.getAbsolutePath(), bmp, null, getActivity()), 260, 300, true);
                    imgList.add(photoPath.toString());
//                    stu_Image.setImageBitmap(bmp);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();

                    Glide.with(getContext()).load(byteArray).asBitmap().override(200,200).fitCenter().diskCacheStrategy(DiskCacheStrategy.RESULT).into(new BitmapImageViewTarget(stu_Image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            stu_Image.setImageDrawable(circularBitmapDrawable);
                        }
                    });

                    imageIsPresent = true;
                    deleteLastImgCaptured(getContext());
                } else {
                    Toast.makeText(getActivity(), "Something went wrong! try again", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {

                imgList.remove(photoPath.toString());
                photoPath = null;

            }
        }
    }

    private void SendMessageTOParents(String mob, String msg) {

        if (!mob.equals("")) {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(mob, null, msg, null, null);
                Toast.makeText(getContext(), "Message Sent",
                        Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getContext(), "Check your Permission", Toast.LENGTH_SHORT).show();
            }


        } else {
            Toast.makeText(getContext(), "Message Not Sent", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveStudentData(final int type) {

        final String mob = et_Student_Search.getText().toString();
        // String hisKey=null;
        final HashMap<String, Object> update_Entry = new HashMap<>();
        String android_id = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);

        final DatabaseReference his_Ref = PersistentDatabase.getDatabase()
                .getReference("student/" + bizId + "/" + grpId + "/" + student_Key);
        final DatabaseReference his_staff_Ref = PersistentDatabase.getDatabase()
                .getReference("access/" + bizId + "/" + grpId + "/" + student_Key);

        final DatabaseReference save_entry_Ref = PersistentDatabase.getDatabase()
                .getReference("hostelHistory/" + bizId + "/" + grpId);
        his_key = save_entry_Ref.push().getKey();


        update_Entry.put("devId", android_id);
        //update_Entry.put("a", false);
        update_Entry.put("mob", student_Mob);
        update_Entry.put("id", student_Key);

        save_entry_Ref.child(in_HisRef).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (inIsChecked) {
                    update_Entry.put("i", type);
                    update_Entry.put("in", System.currentTimeMillis());
                    if (isTypeIsSchool) {
                        update_Entry.put("l", isStudentLate());
                    }
                    if (spForSettings.getInt(Global.TIME_GAP, 0) == 0 && hisRefIsPresent) {
                        his_key = in_HisRef;

                        if (isTypeIsSchool) {
                            his_Ref.child("his_Ref").removeValue();
                        }
                        if (!isTypeIsSchool) {
                            his_staff_Ref.child("his_Ref").removeValue();
                        }

                    } else {
                        if (spForSettings.getInt(Global.TIME_GAP, 0) != 0 &&
                                hisRefIsPresent && CommonMethods.isTimeExceeded(System.currentTimeMillis(),
                                dataSnapshot.child("out").getValue(Long.class),
                                spForSettings.getInt(Global.TIME_GAP, 0) * 60 * 60 * 1000)) {
                            if (isTypeIsSchool) {
                                his_Ref.child("his_Ref").removeValue();
                            }
                            if (!isTypeIsSchool) {
                                his_staff_Ref.child("his_Ref").removeValue();
                            }
                        }
                    }
                }

                if (outIsChecked) {
                    if (hisRefIsPresent) {
                        update_Entry.put("i", IN_ENTRY_MISSED);
                        //  update_Entry.put("in",System.currentTimeMillis());
                        save_entry_Ref.child(in_HisRef).updateChildren(update_Entry);
                        update_Entry.remove("i");
                        //  update_Entry.remove("in");
                    }
                    update_Entry.put("o", type);
                    update_Entry.put("out", System.currentTimeMillis());
                    if (!et_Purpose.getText().toString().trim().equals("")) {
                        update_Entry.put("p", et_Purpose.getText().toString().trim());
                    }

                    if (isTypeIsSchool) {
                        his_Ref.child("his_Ref").setValue(his_key);
                    }
                    if (!isTypeIsSchool) {
                        his_staff_Ref.child("his_Ref").setValue(his_key);
                    }

                }

                save_entry_Ref.child(his_key).updateChildren(update_Entry);

                if (imageIsPresent) {
                    if (photoPath != null) {
                        // imgList.add(photoPath.toString());

                        CommonMethods.uploadImages(getActivity(), mob, photoPath.toString(), bizId, grpId, his_key, student_Key, true, isTypeIsSchool);
                        imgList.clear();
                    } else if (imgUrl != null) {
                        if (isTypeIsSchool) {
                            final DatabaseReference hisRef = PersistentDatabase.getDatabase().
                                    getReference("hostelHistory/" + bizId + "/" + grpId + "/" + his_key);

                            hisRef.child("img").setValue(imgUrl);
                        }

                        if (!isTypeIsSchool) {
                            final DatabaseReference staffHis = PersistentDatabase.getDatabase().
                                    getReference("hostelHistory/" + bizId + "/" + grpId + "/" + his_key);

                            staffHis.child("img").setValue(imgUrl);
                        }
                    }
                }
                et_Student_Search.setText("");
                clearStudentForm();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    public void upDate(String scanContent, int type) {
        if (scanContent.startsWith("#STU|") && isTypeIsSchool) {
            String[] mob = scanContent.split("\\|");
            verificationType = type;
            isVerificationByScan = true;
            et_Student_Search.setText(mob[1]);
            /*

            if (isVerificationByRollNumber)
            {
                verifiesStudentRollNumber(mob[1]);
                btn_Search_Roll_Number.setVisibility(View.GONE);
            }

            */
            searchByStudentRollNumber(mob[1]);
            btn_Submit.setTag(type);
        } else if (scanContent.startsWith("#ST|") && !isTypeIsSchool) {
            String[] mob = scanContent.split("\\|");
            verificationType = type;
            isVerificationByScan = true;
            //et_Student_Search.setText(mob[1]);
            btn_Submit.setTag(type);
            /*if (isVerificationByRollNumber) {
                verifiesStudentRollNumber(mob[1]);
                btn_Search_Roll_Number.setVisibility(View.GONE);
            }*/
            searchByStaffMobileNumber(mob[1]);
        } else {
            Toast.makeText(getActivity(), "Record not found", Toast.LENGTH_SHORT).show();
        }
    }


    private void clearStudentForm() {
        btn_Submit.setEnabled(true);
        ll_Student_Info.setVisibility(View.GONE);
        ll_purpose.setVisibility(View.GONE);
//        tv_sClass.setText("");
        tv_sName.setText("");
        tv_sRoomNumber.setText("");
        et_Purpose.setText("");
        verificationType = Global.MANNUAL_ENTRY;
        hisRefIsPresent = false;
        inIsChecked = false;
        in_HisRef = "";
        outIsChecked = false;
        isStudent_Come_Late = false;
        stu_Image.setImageBitmap(null);
        imageIsPresent = false;
        his_key = "";
        photoPath = null;
        imgUrl = null;
        //  et_Student_Search.setText("");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (resizeBitmapImage != null && !resizeBitmapImage.isRecycled()) {
            resizeBitmapImage.recycle();
            resizeBitmapImage = null;
        }
    }


//  /*  private boolean[] validate()
//    {
//        boolean valid[]=new boolean[2];
//
//        if(et_Purpose.getText().toString().trim().equals(""))
//        {
//            // et_Purpose.setError("Can't be empty");
//            valid[0]=true;
//        }
//        else
//        {
//            et_Purpose.setError(null);
//            valid[0]=true;
//        }
//        if(!imageIsPresent)
//        {
//            valid[1]=false;
//            stu_Image.setImageResource(0);
//            stu_Image.setImageResource(R.drawable.default_user_red);
//        }
//        else
//        {
//            valid[1]=true;
//        }
//
//        return valid;
//
//    }*/

    private boolean validate() {

        /*If photo mandatory setting is true, then only check for validation otherwise not*/
        if (spForSettings.getBoolean(Global.PHOTO_MANDATORY,false)  && !imageIsPresent) {
            stu_Image.setImageResource(0);
            stu_Image.setImageResource(R.drawable.default_user_red);
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {

      /*  if (isVerificationByRollNumber) {
            btn_Search_Roll_Number.setEnabled(true);
            btn_Search_Roll_Number.setVisibility(View.VISIBLE);
        }

        sNumber = et_Student_Search.getText().toString();
        btn_Submit.setTag(type);
        if (sNumber.trim().length() == 10 && isTypeIsSchool) {
            searchByMobileNumber(sNumber);
        } else if (sNumber.trim().length() == 10 && !isTypeIsSchool) {
            searchByStaffMobileNumber(sNumber);
        } else {
            clearStudentForm();
        }*/

        /*If verification by roll number is false, i.e, search by mobile number, then only we've
        * to enable this search....*/
      if(!isVerificationByRollNumber){
          sNumber = et_Student_Search.getText().toString();
          btn_Submit.setTag(type);
          if (sNumber.trim().length() == 10 && isTypeIsSchool) {
              searchByMobileNumber(sNumber);
          } else if (sNumber.trim().length() == 10 && !isTypeIsSchool) {
              searchByStaffMobileNumber(sNumber);
          } else {
              clearStudentForm();
          }
      }else {
          btn_Search_Roll_Number.setEnabled(true);
          btn_Search_Roll_Number.setVisibility(View.VISIBLE);
          clearStudentForm();
      }
    }
}
